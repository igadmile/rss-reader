# Project setup

Clone the repo.

```
git clone https://gitlab.com/igadmile/rss-reader.git
```

# Backend setup

Create **python3** virtual environment and activate it eg:

```
python3 -m virtualenv env
source env/bin/activate
```

Install python dependencies.

```
pip install -r requirements.txt
```

Go to the backend directory and initialize the database.

```
python manage.py migrate
```

(Optional) Create django superuser

```
python manage.py createsuperuser
```

## Add feeds

Add feeds that you want to include into the app by running the `addfeeds` management command. Command accepts command line arguments `--feeds` followed by the list of feeds separated by the space:

```
python manage.py addfeeds --feeds Tech Sport Show Lifestyle
```

## Parse feeds

Parse included feeds and add them into the News table by running `parsefeeds` management command:

```
python manage.py parsefeeds
```

If the command is called again, it will only add news that are published after the last run of the command.

You have to run this command every time you add new feed or if you want to add new news from existing feeds.

## Remove feeds

If you wish to remove added feeds (and news related to them) run the `removefeeds` management command. Command accepts command line arguments `--feeds` followed by the list of feeds separated by the space:

```
python manage.py removefeeds --feeds Tech Sport
```

## Tests

Run the tests by running `python manage.py test api`. If you wish to generate coverage report, run `coverage run --source='.' manage.py test api` and display report by running `coverage report`.

# Frontend setup

## Install dependencies

```
yarn install
```

or

```
npm install
```

# Run the app

Start development server for the frontend in the root directory:

```
yarn serve
```

or

```
npm run serve
```

Start development server for the backend in the backend directory:

```
python manage.py runserver
```

Open the `http://localhost:8080/` in the browser.

If you change the default port (`8000`) for the django app, change it in the `vue.config.js`
