from django.contrib import admin
from .models import Feed, News


class FeedAdmin(admin.ModelAdmin):
    list_display = ('channel', 'url')


class NewsAdmin(admin.ModelAdmin):
    list_display = ('title', 'url', 'channel')


admin.site.register(Feed, FeedAdmin)
admin.site.register(News, NewsAdmin)
