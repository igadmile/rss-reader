from django.core.management.base import BaseCommand
from django.db import IntegrityError
from api.models import Feed


class Command(BaseCommand):
    help = 'Adds RSS feeds to Feeds table'
    feeds = {
        'Aktualno': 'http://www.24sata.hr/feeds/aktualno.xml',
        'Najnovije': 'http://www.24sata.hr/feeds/najnovije.xml',
        'News': 'http://www.24sata.hr/feeds/news.xml',
        'Show': 'http://www.24sata.hr/feeds/show.xml',
        'Sport': 'http://www.24sata.hr/feeds/sport.xml',
        'Lifestyle': 'http://www.24sata.hr/feeds/lifestyle.xml',
        'Tech': 'http://www.24sata.hr/feeds/tech.xml',
        'Viral': 'http://www.24sata.hr/feeds/fun.xml'
    }

    def add_arguments(self, parser):
        parser.add_argument('--feeds', nargs='+',
                            help="Feed names separated by space")

    def _add_feeds(self, feed_name, feed_url):
        feed_model = Feed(channel=feed_name, url=feed_url)

        try:
            feed_model.save()
            self.stdout.write(self.style.SUCCESS(
                'Successfully added feed "%s"' % feed_name))

        except IntegrityError:
            self.stdout.write(self.style.ERROR(
                'Feed "%s" is already in a table. Skipping....' % feed_name))

    def handle(self, *args, **options):
        for option_feed in options['feeds']:
            if option_feed in self.feeds.keys():
                self._add_feeds(option_feed, self.feeds[option_feed])
            else:
                self.stdout.write(self.style.ERROR(
                    'Feed "%s" does not exist. Skipping....' % option_feed))
