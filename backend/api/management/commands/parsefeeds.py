from django.core.management.base import BaseCommand
from django.db import IntegrityError
from api.models import Feed, News
import feedparser
from datetime import datetime
from django.utils import timezone
import re


class Command(BaseCommand):
    help = 'Adds RSS feeds to Feeds table'

    @staticmethod
    def _generate_datetime(feed_entry):
        feed_entry['published'] = datetime.strptime(
            feed_entry['published'], '%a, %d %b %Y %H:%M:%S %z')

        return feed_entry

    @staticmethod
    def _extract_image_summary(feed_entry):
        summary_split = re.search(r'(http.*(?:jpeg|gif|png)).+>\s?(.+)?',
                                  feed_entry['summary'])

        feed_entry['summary'] = summary_split.group(2)
        feed_entry['image'] = summary_split.group(1)

        return feed_entry

    @staticmethod
    def _feed_updated_timedate(feed):
        Feed.objects.filter(
            channel=feed
        ).update(updated=datetime.now(tz=timezone.utc))

    def _clean_feed(self, feeds):
        tidy_date = self._generate_datetime(feeds)
        tidy_image_summary = self._extract_image_summary(tidy_date)

        return tidy_image_summary

    def _add_news(self, feed, news):
        if feed.updated is None or news['published'] > feed.updated:
            try:
                news_model = News(channel=feed, title=news['title'],
                                  url=news['link'],
                                  summary=news['summary'],
                                  image=news['image'],
                                  published=news['published'])

                news_model.save()
                self.stdout.write(self.style.SUCCESS(
                    'Successfully added news "%s"' % news['title']))

            except IntegrityError:
                self.stdout.write(self.style.ERROR(
                    'News "%s" is already in a table. Skipping....' % news['title']))

    def handle(self, *args, **options):
        feeds = Feed.objects.all()

        for feed in feeds:
            parsed_feed = feedparser.parse(feed.url).entries
            for news in parsed_feed:
                cleaned_feed = self._clean_feed(news)
                self._add_news(feed, cleaned_feed)

            self._feed_updated_timedate(feed)
