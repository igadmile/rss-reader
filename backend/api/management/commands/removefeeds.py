from django.core.management.base import BaseCommand
from api.models import Feed


class Command(BaseCommand):
    help = 'Removes RSS feeds from Feed and News table'

    def add_arguments(self, parser):
        parser.add_argument('--feeds', nargs='+',
                            help="Feed names to remove")

    def _remove_feeds(self, feed_name):
        try:
            feed_id = Feed.objects.filter(
                channel=feed_name).values('id')[0]['id']
            Feed.objects.filter(pk=feed_id).delete()

            self.stdout.write(self.style.SUCCESS(
                'Successfully removed feed "%s" from Feed table and News related to the Feed'
                % feed_name))

        except:
            self.stdout.write(self.style.ERROR(
                'Feed "%s" does not exist. Skipping....' % feed_name))

    def handle(self, *args, **options):
        for option_feed in options['feeds']:
            self._remove_feeds(option_feed)
