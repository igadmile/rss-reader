from django.db import models


class Feed(models.Model):
    channel = models.CharField(max_length=10)
    url = models.CharField(max_length=100, unique=True)
    updated = models.DateTimeField(null=True)

    class Meta:
        ordering = ['channel']

    def __str__(self):
        return str(self.channel)


class News(models.Model):
    channel = models.ForeignKey(
        Feed,
        on_delete=models.CASCADE
    )
    title = models.CharField(max_length=100)
    url = models.CharField(max_length=100)
    summary = models.CharField(max_length=100, null=True)
    image = models.CharField(max_length=100, null=True)
    published = models.DateTimeField(null=True)

    class Meta:
        verbose_name_plural = 'news'
        indexes = [
            models.Index(fields=['published']),
        ]
        unique_together = ('channel', 'url')
        ordering = ['-published']

    def __str__(self):
        return str(self.title)
