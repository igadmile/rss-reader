from rest_framework import serializers
from .models import Feed, News


class FeedSerializer(serializers.ModelSerializer):
    class Meta:
        model = Feed
        fields = ('id', 'channel', 'url', 'updated')


class NewsSerializer(serializers.ModelSerializer):
    channel = serializers.CharField(read_only=True)

    class Meta:
        model = News
        fields = ('id', 'channel', 'title', 'url',
                  'summary', 'image', 'published')
