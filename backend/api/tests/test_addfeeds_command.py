from django.test import TestCase
from django.core import management
from io import StringIO
import re


class AddFeedsTests(TestCase):

    @staticmethod
    def remove_ansi_codes(string):
        ansi_escape = re.compile(r'\x1B\[[0-?]*[ -/]*[@-~]')
        return ansi_escape.sub('', string)

    def test_command_proper_input(self):
        out = StringIO()
        management.call_command(
            'addfeeds', feeds=['News', 'Show'], stdout=out)

        self.assertEquals(
            self.remove_ansi_codes(out.getvalue()),
            'Successfully added feed "News"\n'
            'Successfully added feed "Show"\n')

    def test_command_wrong_imput(self):
        out = StringIO()
        management.call_command(
            'addfeeds', feeds=['Test', 'Test2'], stdout=out)

        self.assertEquals(
            self.remove_ansi_codes(out.getvalue()),
            'Feed "Test" does not exist. Skipping....\n'
            'Feed "Test2" does not exist. Skipping....\n')
