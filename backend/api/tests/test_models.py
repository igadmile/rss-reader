from django.db import IntegrityError
from django.test import TestCase
from api.models import Feed, News
import datetime
from django.utils import timezone


class ModelTest(TestCase):
    """ Test module for models """

    def setUp(self):
        self.feed_model = Feed.objects.create(
            channel='News', url='http://www.24sata.hr/feeds/news.xml')

        self.news_model = News.objects.create(
            channel=self.feed_model,
            title='This is test title',
            url='https://www.24sata.hr/',
            summary='Test summary',
            image='Test image',
            published=datetime.datetime.now(tz=timezone.utc))

    def test_feed_str(self):
        feed = Feed.objects.get(pk=1)
        self.assertEqual(
            str(feed), 'News')

    def test_news_str(self):
        news = News.objects.get(pk=1)
        self.assertEqual(
            str(news), 'This is test title')

    def test_feed_integrity(self):
        with self.assertRaises(IntegrityError):
            Feed.objects.create(
                channel='News', url='http://www.24sata.hr/feeds/news.xml')

    def test_news_integrity(self):
        with self.assertRaises(IntegrityError):
            News.objects.create(
                channel=self.feed_model, title='This is test title',
                url='https://www.24sata.hr/',
                summary='Test summary',
                image='Test image',
                published=datetime.datetime.now(tz=timezone.utc))
