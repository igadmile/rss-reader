from django.test import SimpleTestCase
from django.utils import timezone
from api.management.commands.parsefeeds import Command
import datetime as dt

from api.tests.data_test import raw_test_data, control_test_data


class ParseFeedsTests(SimpleTestCase):

    def setUp(self):
        self.command_instance = Command()

    def test_generate_datetime(self):
        datetime = self.command_instance._generate_datetime(raw_test_data)
        test_datetime = dt.datetime(
            2019, 3, 14, 13, 50, tzinfo=dt.timezone.utc)

        self.assertEqual(datetime['published'], test_datetime)

    def test_extract_image_summary(self):
        summary = self.command_instance._extract_image_summary(raw_test_data)

        self.assertEqual(summary['summary'], control_test_data['summary'])
        self.assertEqual(summary['image'], control_test_data['image'])
