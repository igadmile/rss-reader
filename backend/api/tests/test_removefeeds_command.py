from django.test import TestCase
from django.core import management
from io import StringIO
import re


class AddFeedsTests(TestCase):

    def setUp(self):
        management.call_command(
            'addfeeds', feeds=['News', 'Show', 'Tech'])

    @staticmethod
    def remove_ansi_codes(string):
        ansi_escape = re.compile(r'\x1B\[[0-?]*[ -/]*[@-~]')
        return ansi_escape.sub('', string)

    def test_command_proper_input(self):
        out = StringIO()
        management.call_command(
            'removefeeds', feeds=['News', 'Show'], stdout=out)

        self.assertEquals(
            self.remove_ansi_codes(out.getvalue()),
            'Successfully removed feed "News" from Feed table and News related to the Feed\n'
            'Successfully removed feed "Show" from Feed table and News related to the Feed\n')

    def test_command_wrong_imput(self):
        out = StringIO()
        management.call_command(
            'removefeeds', feeds=['Test', 'Test2'], stdout=out)

        self.assertEquals(
            self.remove_ansi_codes(out.getvalue()),
            'Feed "Test" does not exist. Skipping....\n'
            'Feed "Test2" does not exist. Skipping....\n')
