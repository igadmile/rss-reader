from django.test import SimpleTestCase
from django.urls import reverse, resolve
from api.views import FeedView, NewsAlliew, NewsChannelView, NewsSearchView


class TestUrls(SimpleTestCase):
    def test_channels_url_is_resolved(self):
        url = reverse('channels')
        self.assertEquals(resolve(url).func.view_class, FeedView)

    def test_news_url_is_resolved(self):
        url = reverse('news')
        self.assertEquals(resolve(url).func.view_class, NewsAlliew)

    def test_news_by_channel_url_is_resolved(self):
        url = reverse('news-by-channel', kwargs={'channel': 'Tech'})
        self.assertEquals(resolve(url).func.view_class, NewsChannelView)

    def test_news_search_url_is_resolved(self):
        url = reverse('news-search', kwargs={'search': 'this is test'})
        self.assertEquals(resolve(url).func.view_class, NewsSearchView)
