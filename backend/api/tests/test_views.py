from django.test import TestCase
from django.test import Client
from django.urls import reverse
from api.views import FeedView, NewsAlliew, NewsChannelView, NewsSearchView
from api.models import Feed, News
import datetime as dt
from django.utils import timezone


class TestViews(TestCase):
    def setUp(self):
        feed_model = Feed.objects.create(
            channel='News', url='http://www.24sata.hr/feeds/news.xml')

        News.objects.create(
            channel=feed_model,
            title='Title 1',
            url='https://www.24sata.hr/',
            summary='Test summary',
            image='Test image',
            published=dt.datetime(
                2019, 3, 14, 14, 50, tzinfo=dt.timezone.utc))

        News.objects.create(
            channel=feed_model,
            title='Title 2',
            url='https://www.24sata.hr/page2',
            summary='Test summary 2',
            image='Test image 2',
            published=dt.datetime(
                2019, 3, 14, 13, 50, tzinfo=dt.timezone.utc))

        self.expected_channels = [
            {
                'id': 1,
                'channel': 'News',
                'url': 'http://www.24sata.hr/feeds/news.xml',
                'updated': None
            }]

        self.expected_news = [
            {
                'id': 1,
                'channel': 'News',
                'title': 'Title 1',
                'url': 'https://www.24sata.hr/',
                'summary': 'Test summary',
                'image': 'Test image',
                'published': '2019-03-14T14:50:00Z'
            },
            {
                'id': 2,
                'channel': 'News',
                'title': 'Title 2',
                'url': 'https://www.24sata.hr/page2',
                'summary': 'Test summary 2',
                'image': 'Test image 2',
                'published': '2019-03-14T13:50:00Z'
            },
        ]

    def test_get_all_channels(self):
        response = Client().get(reverse('channels'))

        self.assertEqual(response.json(), self.expected_channels)
        self.assertEqual(response.status_code, 200)

    def test_get_all_news(self):
        response = Client().get(reverse('news'))

        self.assertEqual(response.json()['results'], self.expected_news)
        self.assertEqual(response.status_code, 200)

    def test_get_news_by_channel(self):
        url = reverse('news-by-channel', kwargs={'channel': 'News'})
        response = Client().get(url)

        self.assertEqual(response.json()['results'], self.expected_news)
        self.assertEqual(response.status_code, 200)

    def test_get_news_search(self):
        url = reverse('news-search', kwargs={'search': 'Title 1'})
        response = Client().get(url)

        self.assertEqual(response.json()['results'][0], self.expected_news[0])
        self.assertEqual(response.status_code, 200)
