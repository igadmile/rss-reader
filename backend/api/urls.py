from django.urls import path, include
from .views import FeedView, NewsAlliew, NewsChannelView, NewsSearchView


urlpatterns = [
    path('channels/', FeedView.as_view(), name='channels'),
    path('news/', NewsAlliew.as_view(), name='news'),
    path('news/channels/<channel>/',
         NewsChannelView.as_view(), name='news-by-channel'),
    path('news/search/<search>/', NewsSearchView.as_view(), name='news-search'),
]
