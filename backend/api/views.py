from rest_framework.generics import ListAPIView
from django.db.models import Q
from .serializers import FeedSerializer, NewsSerializer
from .models import Feed, News
from .pagination import DefaultPagination


class FeedView(ListAPIView):
    queryset = Feed.objects.all()
    serializer_class = FeedSerializer


class NewsAlliew(ListAPIView):
    queryset = News.objects.all()
    serializer_class = NewsSerializer
    pagination_class = DefaultPagination


class NewsChannelView(ListAPIView):
    serializer_class = NewsSerializer
    pagination_class = DefaultPagination

    def get_queryset(self):
        channel = self.kwargs['channel']
        return News.objects.filter(channel__channel__icontains=channel)


class NewsSearchView(ListAPIView):
    serializer_class = NewsSerializer
    pagination_class = DefaultPagination

    def get_queryset(self):
        search = self.kwargs['search']
        return News.objects.filter(Q(title__icontains=search)
                                   | Q(summary__icontains=search)
                                   | Q(url__icontains=search))
