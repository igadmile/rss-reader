export default {
  methods: {
    handlePaginationChange(url, path) {
      if (this.paginationPage !== 1) {
        this.$store.commit('setPaginationPage', 1);
      } else {
        this.fetchNews(url, path);
      }
    },
  },
};
