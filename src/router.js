import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Channels from './views/Channels.vue';
import SearchView from '@/views/SearchView';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/search',
      name: 'search',
      component: SearchView,
    },
    { path: '/channels/:channel', component: Channels },
  ],
});
