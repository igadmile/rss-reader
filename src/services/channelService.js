import api from '@/services/api';

export default {
  fetchChannels() {
    return api.get(`channels/`).then((response) => response.data);
  },
};
