import api from '@/services/api';

export default {
  fetchAllNews(url) {
    return api.get(url).then((response) => response.data);
  },
};
