import Vue from 'vue';
import Vuex from 'vuex';
import channel from '@/store/modules/channel';
import news from '@/store/modules/news';
import search from '@/store/modules/search';
import pagination from '@/store/modules/pagination';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    channel,
    news,
    search,
    pagination,
  },
});
