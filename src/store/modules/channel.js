import channelService from '@/services/channelService';

export default {
  state: {
    channels: [],
  },
  getters: {
    channelArray(state) {
      return state.channels.map((channel) => channel.channel);
    },
  },
  actions: {
    getChannels({ commit }) {
      channelService.fetchChannels().then((channels) => {
        commit('setChannels', channels);
      });
    },
  },
  mutations: {
    setChannels(state, channels) {
      state.channels = channels;
    },
  },
};
