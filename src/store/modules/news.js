import newsService from '@/services/newsService';

export default {
  state: {
    news: [],
  },
  actions: {
    getNews({ commit }, { url, page }) {
      const apiUrl = `news${url}?page=${page}`;
      newsService.fetchAllNews(apiUrl).then((news) => {
        commit('setNews', news);
      });
    },
  },
  mutations: {
    setNews(state, news) {
      state.news = news;
    },
    removeNews(state) {
      state.news = [];
    },
  },
};
