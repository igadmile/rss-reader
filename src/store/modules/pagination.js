export default {
    state: {
      paginationPage: 1,
    },
    mutations: {
      setPaginationPage(state, page) {
        state.paginationPage = page;
      },
    },
  };
