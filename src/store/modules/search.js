export default {
  state: {
    searchTerm: '',
  },
  mutations: {
    setsearchTerm(state, term) {
      state.searchTerm = term;
    },
  },
};
